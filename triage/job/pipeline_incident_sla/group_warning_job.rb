# frozen_string_literal: true

require_relative './base_job'

module Triage
  module PipelineIncidentSla
    class GroupWarningJob < BaseJob
      DELAY = 1 * 60 * 60 # 1 hour

      SLACK_PAYLOAD_TEMPLATE = <<~MESSAGE.chomp
        This is the 2nd ping on pipeline incident <%<incident_url>s|#%<incident_iid>s>. Please take action following the <%<handbook_url>s|Broken master handbook page>.
        The next ping will go to `#%<stage_channel>s` to request for assistance.
      MESSAGE

      private

      def applicable?
        super && attributed_group
      end

      def threshold_in_seconds
        DELAY
      end

      def slack_channel
        group_slack_channel
      end

      def slack_message
        format(SLACK_PAYLOAD_TEMPLATE,
          incident_iid: event.iid,
          incident_url: event.url,
          handbook_url: TRIAGE_DRI_HANDBOOK_URL,
          stage_channel: stage_slack_channel
        )
      end
    end
  end
end
