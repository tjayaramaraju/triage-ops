# frozen_string_literal: true

require_relative '../change'

module Triage
  class NewRuboCop < Change
    def self.file_patterns
      @file_patterns ||=
        %r{ ^\.rubocop\.ya?ml$
          | ^\.rubocop_todo\.ya?ml$
          | ^\.rubocop_todo/.*\.ya?ml$
          }x
    end

    def self.line_patterns
      @line_patterns ||=
        # To make styles more consistent, disable this cop
        # rubocop:disable Style/RedundantRegexpEscape, Style/RegexpLiteral
        %r{ ^\+\s*Enabled:\s*true      # Add Enabled: true
          | ^\-\s*Enabled:\s*false     # Remove Enabled: false
          }ix
      # rubocop:enable Style/RedundantRegexpEscape, Style/RegexpLiteral
    end
  end
end
