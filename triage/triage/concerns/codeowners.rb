# frozen_string_literal: true

require 'csv'

require_relative '../../triage'

module Triage
  module Codeowners
    CodeownersUrlMissingError = Class.new(StandardError)
    CodeownersUrlInvalidError = Class.new(StandardError)

    CODEOWNERS_URL_VALIDATION_HOST = 'gitlab.com'
    CODEOWNERS_URL_VALIDATION_STRING = 'CODEOWNERS'
    CODEOWNERS_CACHE_EXPIRY = 14400

    EMAIL_REGEXP = /(([^@\s]+@[^@\s]+(?<!\W)))/
    USER_REGEXP = /^@.+/

    def codeowners_data
      ::Triage.cache.get_or_set(self.class.name, expires_in: CODEOWNERS_CACHE_EXPIRY) do
        parse_codeowners
      end
    end

    def paths_for_owner(owner)
      matches = []

      codeowners_data.each do |pattern, owners|
        matches.push(pattern) if owners.include?(owner)
      end

      matches
    end

    private

    # Huge chunks based on https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/7479bfa730b040241e6073bba74d94eecb48562c/lib/code_owners/file.rb
    def parse_codeowners
      parsed = {}

      retrieve_codeowners.lines.each do |line|
        line = line.strip
        next unless line.present?
        next if line.start_with?('#', '^', '[')

        pattern, _separator, owners = line.partition(/(?<!\\)\s+/)

        pattern = normalize_pattern(pattern)

        parsed[pattern] = normalized_owners(owners)
      end

      parsed
    end

    def normalize_pattern(pattern)
      # Remove `\` when escaping `\#`
      pattern = pattern.sub(/\A\\#/, '#')
      # Replace all whitespace preceded by a \ with a regular whitespace
      pattern.gsub(/\\\s+/, ' ')
    end

    def normalized_owners(owners)
      owners.split(/\s+/).select do |owner|
        owner.match?(EMAIL_REGEXP) || owner.match?(USER_REGEXP)
      end
    end

    def retrieve_codeowners
      body = HTTParty.get(codeowners_url)
      body.parsed_response
    end

    def codeowners_url
      self.class.const_get(:CODEOWNERS_FILE).tap do |url|
        validate_url!(url)
      end
    end

    def validate_url!(url)
      uri = begin
        URI.parse(url)
      rescue URI::InvalidURIError
        raise CodeownersUrlMissingError, "CODEOWNERS Url is missing. The CODEOWNERS_FILE constant needs to be set."
      end

      raise CodeownersUrlInvalidError, 'CODEOWNERS Url is invalid: CODEOWNERS Url is for the wrong host' unless uri.host == CODEOWNERS_URL_VALIDATION_HOST
      raise CodeownersUrlInvalidError, 'CODEOWNERS Url is invalid: CODEOWNERS Url looks incorrect' unless url.include?(CODEOWNERS_URL_VALIDATION_STRING)
    end
  end
end
