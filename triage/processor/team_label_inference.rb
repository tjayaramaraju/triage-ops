# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../../lib/devops_labels'

module Triage
  class TeamLabelInference < Processor
    include DevopsLabels::Context

    react_to 'issue.open', 'issue.reopen', 'issue.update',
      'merge_request.open', 'merge_request.reopen', 'merge_request.update'

    # Applicable when a section, stage or group label is present/changed
    # @return [Boolean] true when
    def applicable?
      event.from_gitlab_org? &&
        !event.gitlab_bot_event_actor? &&
        can_infer_labels?
    end

    def process
      comment = comment_for_intelligent_stage_and_group_labels_inference
      return unless comment
      return if comment.empty?

      add_comment(comment, append_source_link: false)
    end

    def documentation
      <<~TEXT
        Section, Stage and Group labels must align.

        Group will always take priority over Stage and Section labels. This will allow for the least amount of
        changes. I.e., when a Group label is added/updated, Stage and Section labels are updated.

        - When no Group Stage or Section labels exist: Do nothing
        - When only a Section label is present: Do nothing
        - When only a Group label is present: Infer Stage and Section label based on the Group
        - When only a Stage label is present: Infer Section label based on the Stage
        - When only Stage and Group labels are present
          - If the Stage and Group labels match: Only infer the Section label
          - If the Stage and Group labels mismatch: Change the Stage and Section labels based on the Group
        - When only Section and Group labels are present
          - If the Section and Group labels match: Only infer the Stage label
          - If the Section and Group labels mismatch: Change the Section and Stage labels based on the Group
        - When all Section, Stage and Group labels are present
          - If the Section does not match the Group: Change the Section label based on the Group
          - If the Stage does not match the Group: Change the Stage label based on the Group

        Examples:
          Good:
            "section::dev" "devops::create" "group::source code"
            "section::dev" "devops::plan" "group::ide"

          Bad:
            "section::ops" "devops::create" "group::source code" #=> Problem: Incorrect Section; Action: change section::ops to section::dev
            "devops::create" "group::source code" #=> Problem: No Section; Action: add section::dev
            "section::ops" #=> Problem: Missing Stage and Group labels; Action: Leave a note for adding Stage and Group labels
            "section::dev" "devops::create" #=> Problem: Missing Group label; Action: Do nothing.
            "devops::plan" "group::ide" => Problem: No Section, Incorrect Stage; Action: add section::dev, change devops::plan to devops::create

        Note: This processor depends on a valid Enterprise License to support Scoped Labels.  If no EE
              license is present, you may receive two identical comments during an update.
      TEXT
    end

    private

    def label_names
      @label_names ||= event.label_names
    end
  end
end
