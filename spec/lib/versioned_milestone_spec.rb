# frozen_string_literal: true

require 'spec_helper'
require_relative '../../lib/versioned_milestone'

RSpec.describe VersionedMilestone do
  subject { described_class.new(context) }

  around do |example|
    Timecop.freeze(2020, 0o3, 31) { example.run }
  end

  let(:context) do
    Gitlab::Triage::Resource::Context.build(resource, redact_confidentials: false)
  end

  let(:resource) { { type: 'issue', milestone: milestone_resource } }
  let(:root_milestone) { double }
  let(:milestone_resource) { nil }

  let(:all_non_expired) do
    [
      { title: '12.10', start_date: '2020-3-18', due_date: '2020-4-17' },
      { title: '13.0', start_date: '2020-4-18', due_date: '2020-5-17' }
    ].map { |hash| Gitlab::Triage::Resource::Milestone.new(hash) }
  end

  let(:all_active_with_start_date) do
    [
      { title: '12.7', start_date: '2019-12-18', due_date: '2020-1-17' },
      { title: '12.8', start_date: '2020-1-18', due_date: '2020-2-17' },
      { title: '12.9', start_date: '2020-2-18', due_date: '2020-3-17' }
    ].map { |hash| Gitlab::Triage::Resource::Milestone.new(hash) } + all_non_expired
  end

  let(:all_active_with_start_date_from_api) do
    [
      { title: 'FY25', start_date: '2025-1-1' }
    ].map { |hash| Gitlab::Triage::Resource::Milestone.new(hash) } + all_active_with_start_date
  end

  before do
    allow(subject).to receive(:root_milestone).and_return(root_milestone)
    allow(root_milestone).to receive(:all_active_with_start_date).and_return(all_active_with_start_date_from_api)
  end

  describe '#all_active_with_start_date' do
    it 'does not return FY25' do
      expect(subject.all_active_with_start_date.map(&:title)).to eq(all_active_with_start_date.map(&:title))
    end
  end

  describe '#all_non_expired' do
    it 'does not return 12.7, 12.8, 12.9' do
      expect(subject.all_non_expired.map(&:title)).to eq(all_non_expired.map(&:title))
    end
  end

  describe '#current' do
    it 'returns the first from all_non_expired' do
      expect(subject.current.title).to eq('12.10')
    end
  end

  describe '#next' do
    it 'returns the second from all_non_expired' do
      expect(subject.next.title).to eq('13.0')
    end
  end

  describe '#find_milestone_for_date' do
    let(:found_milestone) { subject.find_milestone_for_date(date) }

    context 'when date is equal to start_date' do
      let(:date) { Date.parse('2019-12-18') }

      it 'returns the first from all' do
        expect(found_milestone.title).to eq('12.7')
      end
    end

    context 'when date is equal to due_date' do
      let(:date) { Date.parse('2020-2-17') }

      it 'returns the first from all' do
        expect(found_milestone.title).to eq('12.8')
      end
    end

    context 'when date is between start_date and due_date' do
      let(:date) { Date.parse('2020-2-29') }

      it 'returns the first from all' do
        expect(found_milestone.title).to eq('12.9')
      end
    end

    context 'when date does not match any milestone' do
      let(:date) { Date.parse('2020-5-29') }

      it 'returns the first from all' do
        expect(found_milestone).to be_nil
      end
    end

    describe '#current?' do
      context 'when matching current milestone' do
        let(:other_milestone) { double(title: '12.10') }

        it 'returns true' do
          expect(subject.current?(other_milestone)).to be(true)
        end
      end

      context 'when mismatched milestone' do
        let(:other_milestone) { double(title: '12.11') }

        it 'returns false' do
          expect(subject.current?(other_milestone)).to be(false)
        end
      end

      context 'when nil milestone' do
        let(:other_milestone) { nil }

        it 'returns false' do
          expect(subject.current?(other_milestone)).to be(false)
        end
      end
    end
  end
end
