# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/mr_approved_label'
require_relative '../../triage/triage/event'

RSpec.describe Triage::MrApprovedLabel do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:merge_request_iid) { 300 }
    let(:approvers) { [] }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approved',
        from_gitlab_org_gitlab?: true,
        team_member_author?: true,
        automation_author?: false,
        jihu_contributor?: false,
        source_branch_is?: false,
        target_branch_is_stable_branch?: false,
        approvers: approvers,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  subject { described_class.new(event) }

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "lib/gitlab.rb",
          "new_path" => "lib/gitlab.rb"
        }
      ]
    }
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  include_examples 'registers listeners', [
    'merge_request.approval',
    'merge_request.approved',
    'merge_request.unapproval',
    'merge_request.unapproved',
    'merge_request.update'
  ]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event project is under gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(true)
      end

      include_examples 'event is applicable'
    end

    context 'when event project is under gitlab-org/security/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_security_gitlab?)
          .and_return(true)
      end

      include_examples 'event is applicable'
    end

    describe 'need_mr_approved_label?' do
      before do
        fake_instance = instance_double(Triage::NeedMrApprovedLabel)
        allow(Triage::NeedMrApprovedLabel).to receive(:new).and_return(fake_instance)
        allow(fake_instance).to receive(:need_mr_approved_label?).and_return(need_mr_approved_label_value)
      end

      context 'when need_mr_approved_label? is true' do
        let(:need_mr_approved_label_value) { true }

        include_examples 'event is applicable'
      end

      context 'when need_mr_approved_label? is false' do
        let(:need_mr_approved_label_value) { false }

        include_examples 'event is not applicable'
      end
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    before do
      stub_api_request(
        path: "/projects/12345/issues/6789/notes",
        query: { per_page: 100 },
        response_body: []
      )
    end

    context 'when there are approvers on the MR' do
      let(:approvers) { [{ 'id' => 567, 'username' => 'julie' }] }

      it 'posts a comment' do
        body = <<~MARKDOWN.chomp
          /label ~\"#{Labels::MR_APPROVED_LABEL}\"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when there are no approvers on the MR' do
      let(:approvers) { [] }

      it 'keeps the mr-approved label' do
        body = <<~MARKDOWN.chomp
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end
  end
end
