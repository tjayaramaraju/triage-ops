# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/growth_team/add_thread_on_refinement'
require_relative '../../../triage/triage/event'
require_relative '../../../lib/constants/labels'

RSpec.describe Triage::AddThreadOnRefinement do
  include_context 'with event', Triage::IssueEvent do
    let(:event_attrs) do
      {
        from_gitlab_org?: true,
        added_label_names: [Labels::WORKFLOW_REFINEMENT_LABEL],
        label_names: Labels::GROWTH_TEAM_LABELS
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['issue.update', 'issue.open']

  include_examples 'applicable on contextual event'

  describe '#applicable?' do
    context 'when issue does not belong to growth team' do
      before do
        allow(event).to receive(:added_label_names).and_return(Labels::THREAT_INSIGHTS_TEAM_LABELS)
        allow(event).to receive(:label_names).and_return(Labels::THREAT_INSIGHTS_TEAM_LABELS)
      end

      include_examples 'event is not applicable'
    end

    context "when 'workflow::refinement' label is not added" do
      before do
        allow(event).to receive(:added_label_names).and_return(Labels::GROWTH_TEAM_LABELS)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    it 'posts a comment' do
      body = add_automation_suffix do
        <<~MARKDOWN.chomp
      ## :construction: Refinement
      Welcome to the refinement stage for this issue. During this stage, engineers will ensure that this issue meets our criteria for issues ready for development.

      Please follow these guidelines to ensure a thorough refinement:
      - Verify the issue description and check that the requirements are present and well-defined
      - Evaluate risks and dependencies
      - Uncover blind spots if possible
      - Outline possible technical solutions for the problem

      :warning: Make sure every outcome of the refinement discussion is included in the issue's description. We want to avoid needing to go over multiple comments and threads to figure out what is the decision or additional requirement. The description should be the single source of truth when it comes to implementation details.

      As an engineer, please:
      - Discuss and update the issue description (if needed)
      - Apply a weight estimate by voting :one: :two: :three: :five: or :rocket: (if above 5) - :rocket: means it's likely too big and needs splitting into smaller issues (also please suggest how by starting a discussion)
      - Ensure the issue has a `~type` label

      This issue will be considered as completed with refinement if it receives 3 unique weight estimations, and moved to ~"workflow::scheduling" afterwards.

      /cc @gitlab-org/growth/engineers
        MARKDOWN
      end

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
