# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/event'
require_relative '../../triage/triage/need_mr_approved_label'

RSpec.describe Triage::NeedMrApprovedLabel, :clean_cache do
  let(:instance) { described_class.new(event) }
  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "lib/gitlab.rb",
          "new_path" => "lib/gitlab.rb"
        }
      ]
    }
  end

  include_context 'with event', Triage::MergeRequestEvent do
    let(:label_names) { [] }
    let(:merge_request_iid) { 300 }
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org_gitlab?: true,
        team_member_author?: true,
        automation_author?: false,
        jihu_contributor?: false,
        source_branch_is?: false,
        target_branch_is_stable_branch?: false,
        iid: merge_request_iid,
        project_id: project_id,
        label_names: label_names
      }
    end
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  describe '#need_mr_approved_label?' do
    subject { instance.need_mr_approved_label? }

    context 'when `pipeline::expedited` is set' do
      let(:label_names) { ['pipeline::expedited'] }

      it { is_expected.to be_falsey }
    end

    context 'when `pipeline:expedite` is set' do
      let(:label_names) { ['pipeline:expedite'] }

      it { is_expected.to be_falsey }
    end

    context 'when merge request source branch is `release-tools/update-gitaly`' do
      before do
        allow(event).to receive(:source_branch_is?).with(described_class::UPDATE_GITALY_BRANCH).and_return(true)
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request target branch is a stable branch' do
      before do
        allow(event).to receive(:target_branch_is_stable_branch?).and_return(true)
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request contains only docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request contains only qa changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            }
          ]
        }
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request contains only templates changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => ".gitlab/issue_templates/Deprecations.md",
              "new_path" => ".gitlab/issue_templates/Deprecations.md"
            }
          ]
        }
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request contains only qa, docs, and templates changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            },
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            },
            {
              "old_path" => ".gitlab/issue_templates/Deprecations.md",
              "new_path" => ".gitlab/issue_templates/Deprecations.md"
            }
          ]
        }
      end

      it { is_expected.to be_falsey }
    end

    context 'when merge request contains a mixture of docs and non-docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "lib/gitlab.rb",
              "new_path" => "lib/gitlab.rb"
            },
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      it { is_expected.to be_truthy }
    end

    context 'when merge request contains a mixture of qa and non-qa changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "lib/gitlab.rb",
              "new_path" => "lib/gitlab.rb"
            },
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            }
          ]
        }
      end

      it { is_expected.to be_truthy }
    end
  end
end
