# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../../triage/triage/event'
require_relative '../../../../triage/triage/pipeline_failure/config/approved_mr'

RSpec.describe Triage::PipelineFailure::Config::ApprovedMr do
  let(:instance) { :com }
  let(:project_path_with_namespace) { 'gitlab-org/gitlab' }
  let(:ref) { 'master' }
  let(:source_job_id) { nil }
  let(:merge_request_pipeline?) { true }
  let(:merge_request_approved?) { true }
  let(:status) { 'failed' }
  let(:duplicate?) { true }
  let(:event) do
    instance_double(Triage::PipelineEvent,
      instance: :com,
      project_path_with_namespace: project_path_with_namespace,
      project_id: 42,
      id: 12,
      ref: ref,
      merge_request_pipeline?: merge_request_pipeline?,
      merge_request_approved?: merge_request_approved?,
      source_job_id: source_job_id,
      status: status)
  end

  let(:first_job) { Gitlab::ObjectifiedHash.new('id' => 1, 'name' => 'first', 'web_url' => 'first_url', 'allow_failure' => false) }
  let(:second_job) { Gitlab::ObjectifiedHash.new('id' => 2, 'name' => 'second', 'web_url' => 'second_url', 'allow_failure' => false) }
  let(:api_client_double) { instance_double(Gitlab::Client) }
  let(:incident_title) { 'Wednesday 2023-09-27 17:47 UTC - `gitlab-org/gitlab` broken `master` with first, second, third' }
  let(:incidents) { [Gitlab::ObjectifiedHash.new('web_url' => "https://gitlab.com/group/project/-/issues/2", 'title' => incident_title, '_links' => {})] }
  let(:triager) { instance_double(Triage::PipelineFailure::TriageIncident) }
  let(:failed_jobs) { instance_double(Triage::PipelineFailure::FailedJobs) }

  subject { described_class.new(event) }

  describe '.match?' do
    before do
      allow(event).to receive(:on_instance?).with(:com).and_return(true)

      allow(Triage::PipelineFailure::TriageIncident).to receive(:new).and_return(triager)
      allow(triager).to receive(:duplicate?).and_return(duplicate?)
      allow(Triage::PipelineFailure::FailedJobs).to receive(:new).and_return(failed_jobs)
      allow(failed_jobs).to receive(:execute).and_return([first_job, second_job])

      allow(api_client_double).to receive(:issues).and_return(incidents)
    end

    it 'returns true' do
      expect(described_class.match?(event)).to be_truthy
    end

    context 'when event is not on the gitlab.com instance' do
      before do
        allow(event).to receive(:on_instance?).with(:com).and_return(false)
      end

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when project_path_with_namespace is not "gitlab-org/gitlab"' do
      let(:project_path_with_namespace) { 'foo/bar' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when source_job_id is present' do
      let(:source_job_id) { '42' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when not triggered by a merge request pipeline' do
      let(:merge_request_pipeline?) { false }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when MR is not approved' do
      let(:merge_request_approved?) { false }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when pipeline status isnot "failed"' do
      let(:status) { 'success' }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end

    context 'when incident is not a duplicate' do
      let(:duplicate?) { false }

      it 'returns false' do
        expect(described_class.match?(event)).to be_falsy
      end
    end
  end

  describe '#incident_project_id' do
    it 'returns expected projet id' do
      expect(subject.incident_project_id).to eq('54990218')
    end
  end

  describe '#canonical_incident_project_id' do
    it 'returns expected canonical incident projet id' do
      expect(subject.canonical_incident_project_id).to eq(Triage::Event::MASTER_BROKEN_INCIDENT_PROJECT_ID)
    end
  end

  describe '#incident_template' do
    it 'returns expected template' do
      expect(subject.incident_template).to eq(described_class::INCIDENT_TEMPLATE)
    end
  end

  describe '#incident_labels' do
    it 'returns expected labels' do
      expect(subject.incident_labels).to eq(['Engineering Productivity'])
    end
  end

  describe '#default_slack_channels' do
    it 'returns expected channel' do
      expect(subject.default_slack_channels).to eq(['mr-blocked-by-master-broken'])
    end
  end

  describe '#duplicate_command_body' do
    it 'returns expected command body' do
      expect(subject.duplicate_command_body).to eq(described_class::DEFAULT_DUPLICATE_COMMAND_BODY)
    end
  end
end
