# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/react_to_changes'
require_relative '../../../triage/triage/react_to_changes/add_as_if_jh'

RSpec.describe Triage::AddAsIfJH do
  include_context 'with event', Triage::MergeRequestEvent do
    let(:event_attrs) do
      {
        object_kind: 'merge_request'
      }
    end

    let(:gemfile_change) do
      {
        'old_path' => 'Gemfile.lock',
        'new_path' => 'Gemfile.lock',
        'diff' => '+ gem "triage-ops"'
      }
    end

    let(:feature_flag_config_change) do
      {
        'old_path' => 'config/feature_flags/ops/api.yml',
        'new_path' => 'config/feature_flags/ops/api.yml',
        'diff' => '+ default_enabled: true'
      }
    end

    let(:merge_request_changes) do
      { 'changes' => [gemfile_change, feature_flag_config_change].compact }
    end
  end

  let(:processor) { Triage::ReactToChanges.new(event) }

  subject { described_class.new(processor) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{iid}/changes",
      response_body: merge_request_changes)
  end

  describe '#applicable?' do
    include_examples 'applicable on contextual event'

    context 'when there is no dependency changes' do
      let(:gemfile_change) { nil }

      it 'is not applicable' do
        expect(subject).not_to be_applicable
      end
    end

    context 'when there is no feature flag config changes' do
      let(:feature_flag_config_change) { nil }

      it 'is not applicable' do
        expect(subject).not_to be_applicable
      end
    end
  end

  describe '#react' do
    it 'adds a comment to add the ~"pipeline:run-as-if-jh" label' do
      expect(processor).to receive(:add_comment)
        .with('/label ~"pipeline:run-as-if-jh"', append_source_link: false)

      subject.react
    end
  end
end
