# frozen_string_literal: true

module StubRequests
  def expect_api_requests
    requests = []

    yield requests

    expect(requests).to all(have_been_requested)
  ensure
    WebMock.reset_executed_requests!
  end

  def expect_api_request(times: 1, **arguments)
    request = stub_api_request(**arguments)

    yield

    expect(request).to have_been_requested.times(times)
  ensure
    WebMock.reset_executed_requests!
  end

  def expect_comment_request(times: 1, **arguments)
    request = stub_comment_request(**arguments)

    yield

    expect(request).to have_been_requested.times(times)
  ensure
    WebMock.reset_executed_requests!
  end

  def expect_discussion_request(times: 1, **arguments)
    request = stub_discussion_request(**arguments)

    yield

    expect(request).to have_been_requested.times(times)
  ensure
    WebMock.reset_executed_requests!
  end

  def expect_discussion_notes_request(times: 1, **arguments)
    request = stub_discussion_notes_request(**arguments)

    yield

    expect(request).to have_been_requested.times(times)
  ensure
    WebMock.reset_executed_requests!
  end

  def expect_no_request(**arguments)
    request = stub_api_request(**arguments)

    yield

    expect(request).not_to have_been_requested
  ensure
    WebMock.reset_executed_requests!
  end

  def stub_api_request(verb: :get, path: nil, headers: {}, query: nil, request_body: nil, response_body: {}, response_headers: {}, response_status: 200)
    request_params = { headers: headers.merge('Private-Token': api_token) }
    request_params[:query] = query
    request_params[:body] = request_body

    response_headers = { 'content-type' => 'application/json' }.merge(response_headers)

    response_body = response_body.to_json if response_headers['content-type'] == 'application/json'

    stub_request(verb, "#{Triage.api_endpoint(:com)}#{path}")
      .with(request_params)
      .to_return(body: response_body, status: response_status, headers: response_headers)
  end

  def stub_comment_request(body:, event: nil, noteable_path: nil, **options)
    noteable_path ||= event.noteable_path
    stub_api_request(verb: :post, path: "#{noteable_path}/notes", request_body: { 'body' => body }, **options)
  end

  def stub_discussion_request(body:, event: nil, noteable_path: nil, **options)
    noteable_path ||= event.noteable_path
    stub_api_request(verb: :post, path: "#{noteable_path}/discussions", request_body: { 'body' => body }, **options)
  end

  def stub_discussion_notes_request(body:, event: nil, noteable_path: nil, discussion_id: nil, **options)
    noteable_path ||= event.noteable_path
    discussion_id ||= event.payload.dig('object_attributes', 'discussion_id')
    stub_api_request(verb: :post, path: "#{noteable_path}/discussions/#{discussion_id}/notes", request_body: { 'body' => body }, **options)
  end

  def api_token
    'api'
  end

  def webhook_token
    'webhook'
  end

  def add_automation_suffix(body = nil, source_path: nil)
    body = yield if block_given?

    # Infer source path from spec location.
    # Convert <triage-ops path>/spec/job/scheduled_issue_type_label_nudger_job_spec.rb
    # to job/scheduled_issue_type_label_nudger_job.rb
    source_path ||= caller_locations[0]
      .path
      .delete_prefix(File.expand_path('..', __dir__))
      .delete_prefix('/')
      .sub('_spec.rb', '.rb')

    <<~MARKDOWN.chomp
      #{body}

      *This message was [generated automatically](https://handbook.gitlab.com/handbook/engineering/infrastructure/engineering-productivity/triage-operations).
      You're welcome to [improve it](https://gitlab.com/gitlab-org/quality/triage-ops/-/blob/master/triage/#{source_path}).*
    MARKDOWN
  end
end
