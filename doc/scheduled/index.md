# Scheduled operations

[[_TOC_]]

Triage-ops scheduled operations are powered by <https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage>.

## Schedules

The purpose of schedules is to run pipelines that execute policies periodically, which create or operate on GitLab resources (issues, merge requests, and epics).

Triage-ops schedules are **GitLab scheduled pipelines**. They can be found in the triage-ops project at https://gitlab.com/gitlab-org/quality/triage-ops/-/pipeline_schedules. Schedules are defined in the `pipeline-schedules.yml` file.

Schedules are managed automatically by the `schedules-sync` job which runs for
every non-scheduled `master` commit (a `dry-run:schedules-sync` job also runs on
any non-`master` pipeline).

These jobs run the `bin/manage_schedules` script, which allows to sync pipeline
schedules and their variables. Additionally, it will automatically disable all the pipeline schedules that
aren't declared.

### Setting up a new schedule

Creating a scheduled pipeline is done programmatically by modifying the `pipeline-schedules.yml` file.

Please note that there is a limit to the [number of pipeline schedules](https://docs.gitlab.com/ee/administration/instance_limits.html#number-of-pipeline-schedules) allowed in a project, which is defined at the plan level, not at the project level, therefore only plan admins can make changes to it and the change will impact all projects under that plan. Check existing pipeline schedules at https://gitlab.com/gitlab-org/quality/triage-ops/-/pipeline_schedules before adding a new one to avoid the `Maximum number of ci pipeline schedules (50) exceeded` error. Right now the limit is set to 50.

### Format of `pipeline-schedules.yml`

The format of the `pipeline-schedules.yml` file is as follows:

```yaml
---
gitlab-org:
  base:
    cron: '0 4 * * 1-5'
    variables:
      TRIAGE_SOURCE_TYPE: groups
      TRIAGE_SOURCE_PATH: 9970
  variants:
    - id: 11219
      ref: 'feature'
      cron: '0 4 * * 1-7'
      cron_timezone: 'Europe'
      active: true
      variables:
        TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS: 1
    - active: false
      description: 'gitlab-org weekly schedule'
      cron: '0 0 * * 1'
      variables:
        TRIAGE_TEAM_SUMMARY: 1
```

1. `base` are shared attributes and variables for all the variants of a schedule.
   - Supported attributes are `ref`, `cron`, `cron_timezone`, `active`.
1. `variants` is an array of variants for which you can define/override attributes
    and variables (e.g. useful to define a daily and a weekly job for the same group/project)
   - Supported attributes are the ones from `base`, and additionally `id` and `description`.
   - Defining an `id` allows to update an existing pipeline schedule `description`,
     otherwise it would be impossible since pipeline schedules are matched based on
     their description if no `id` is provided.
1. If `description` is omitted, it will use a default description of
   `[MANAGED] <schedule key> (<variables list>)`, e.g. for the first variant in
   the above example, that would be `[MANAGED] 'gitlab-org' ('TRIAGE_LABEL_COMMUNITY_CONTRIBUTIONS=1')`.
1. Other attributes also have a default value:
   - `ref='master'`
   - `cron_timezone='UTC'`
   - `cron='0 0 * * 1-5'` (Monday to Friday at midnight)
   - `active=true`
1. To temporarily disable a pipeline schedule, just set `active: false`.

### Testing a schedule with a dry-run pipeline

Similar to testing with a dry-run for a policy file, we can also trigger a
pipeline using the same variables configured in the schedule.

1. Open the `dry-run:schedule` job in your merge request pipeline.
1. Enter `TRIAGE_SCHEDULE_NAME` variable with the path of schedule you want to test, for instance:
   - Fill the `Key` field with `TRIAGE_SCHEDULE_NAME`.
   - Fill the `Value` field with `gitlab-org/build/cng`
1. (Optional) If there are multiple schedules with the same name, we can also
   pass `TRIAGE_SCHEDULE_INDEX` to indicate which one we want to run.
   By default, it's the first one, which the index is `0`.
1. (Optional) If we want to add some additional variables to the triggered
   pipeline, we can prefix those variables with `BYPASSING_` so they will be
   passed along to the triggered pipeline without the prefix. For example,
   suppose we want to add `TRIAGE_FAKE_TODAY_FOR_MISSED_RESOURCES=2020-01-01`
   to the triggered pipeline, we can:
   - Fill the `Key` field with `BYPASSING_TRIAGE_FAKE_TODAY_FOR_MISSED_RESOURCES`.
   - Fill the `Value` field with `2020-01-01`
1. This will use the same variables defined as in the schedule, along with `DRY_RUN=1` to make sure everything should be running in dry-run mode.
1. Click "Trigger this manual action" and you can see the triggered pipeline
  URL printed in the job log. You can also go to the same pipeline page and
  check the triggered downstream pipeline.

### Special schedules

#### Triage Reports

Triage Reports are schedules that will create an issue. This issue will generally contain many issues for teams/groups/stages to triage.

## Policies

Policies are YAML files that allow us to manage GitLab resources (issues, merge requests, and epics). A schedule runs a pipeline to execute policies. Policies are explained at: [What is a triage policy?](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage#what-is-a-triage-policy).

### Setting up a new policy

Policies are found in [the policies folder](./../../policies). There are subfolders to run a policy for an entire stage/group/team, specific to a project, etc.

### Testing policies with a dry-run

When creating or changing a policy, you are able to test it by leveraging the pipelines within your merge request.
Here is a [video](https://youtu.be/AtsbQgx87yo) showing how to test a policy using the `dry-run:custom` job.

#### Preconditions

1. If you are changing or creating a new policy, be sure to open your merge request first as DRAFT.
1. Ensure there is data available for your test condition.

#### Steps

1. Open the `dry-run:custom` job in your merge request pipeline.
1. Enter at least the `TRIAGE_POLICY_FILE` variable with the path of the policy file you want to test, for instance:
   - Fill the `Key` field with `TRIAGE_POLICY_FILE`.
   - Fill the `Value` field with `policies/stages/hygiene/label-missed-slo.yml`.
1. By default the dry-run will run against the `gitlab-org/gitlab` project. If you want to change that, you can add:
   - A `TRIAGE_SOURCE_TYPE` variable with the value `projects` (default) or `groups`.
   - A `TRIAGE_SOURCE_PATH` variable with the path or ID of the project or group that you want to test the new policy against.
1. Click "Trigger this manual action" and enjoy! 🍿

### Special policies

#### Generated Policies

Some policies are templated, and need to be rendered. The `policies/generated` contains the end-result policies that we will apply to different resources.

Due to some [technical limitation](https://gitlab.com/gitlab-org/gitlab-triage/-/issues/191),
it's much easier to generate policy files and CI jobs via scripts.

We once [tried to generate a child pipeline](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/483#note_337672436)
to run the generated CI jobs, but it turned out a child pipeline cannot
generate another child pipeline, yet. Thus it's much easier if we just
commit them into the repository.

Suppose we want to generate the CI jobs and policies for *Collate merge
requests requiring attention*, we can run below:

```shell
bin/generate_group_policies --template policies/template/group/merge-requests-needing-attention.yml.erb
bin/generate_ci_jobs --template .gitlab/ci/template/merge-requests-needing-attention.yml.erb
```

We have more templates than just that though. In order to regenerate everything, we should run the script in a local copy of this project:

```shell
bin/generate_all_policies_and_jobs
```

There is a `regeneration-check` job to verify if generated files are all
up-to-date. The job will run the same to iterate through all the templates,
and check if it's identical to what were committed.

#### One-off Policies

In some scenarios, in addition to creating/renaming label titles, we need to run one-off policies to migrate existing labels.

Examples:
1. Merge type labels. See [this merge requst](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/1201).
1. Move a group from one section to another, or any changes to the result of team label inference. See [this merge request](https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/1578)

Following are a few guidelines for these one-off policies:

1. Have the one-off policy merge request ready as soon as a label migration request has been submitted, but wait until changes are merged to the [Product sections, stages, groups, and categories](https://about.gitlab.com/handbook/product/categories/) page before running the one-off policy.
1. These policies should be created in the `policies/one-off/` folder.
1. Dry-run and actual run jobs should be added to the `.gitlab/ci/one-off.yml` file.
1. For convenience, the `TRIAGE_POLICY_FILE` variable will automatically be set based on the job name.
   For instance, the `sample-one-off-migration:dry-run` and `sample-one-off-migration` jobs
   would have `TRIAGE_POLICY_FILE="policies/one-off/sample-one-off-migration.yml"` set automatically.
1. Merge requests for one-off policies should have the ~"one-off" label set, and should not be merged. Close it without merging once the policies have been run.

Note:

It's known GitLab API often returns 500 Internal Server error when automation
is processing a lot of resources. This is usually due to database query
timeout, like in this issue:
[Internal server error in the `testcases:default-labels` job](https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/655)

When this happens, make sure the one-off policies can run progressively, and
feel free to retry it until it processes through everything. If it takes too
many attempts manually, we can prepend `seq 10 | xargs -I{}` to the command
running the one-off policies and force it to run 10 times in a row. This
should save some manual attempts. See this for an example:
<https://gitlab.com/gitlab-org/quality/triage-ops/-/merge_requests/1409#note_966358955>

## Troubleshooting executed jobs and pipelines

### In the CI

After a scheduled pipeline has been executed, the link to the latest pipeline for that schedule can be found in the [pipeline schedules list](https://gitlab.com/gitlab-org/quality/triage-ops/-/pipeline_schedules). This can be a good way to find the latest instance of a particular pipeline or job associated that executes a particular rule.

For generated triage reports, they often contain a URL to the job that created them. This can be useful to find out more about the number of issues discovered by the rule's query or to determine at what time of day a rule was executed.

### Locally

Run a policy in dry-run:

```shell
cd ~/src/triage-ops

# Change to the policy you'd like to execute
export TRIAGE_POLICY_FILE=policies/generated/team-summary.yml.erb/import_and_integrate.yml

export TRIAGE_SOURCE_TYPE=groups # `projects` or `groups`
export TRIAGE_SOURCE_PATH=9970   # gitlab-org group

export GITLAB_BOT_API_TOKEN_ON_COM="${GITLAB_API_PRIVATE_TOKEN}"
export EXTRA_FLAGS="--dry-run"

bundle exec gitlab-triage -r ./plugins/all --debug -f $TRIAGE_POLICY_FILE --token $GITLAB_BOT_API_TOKEN_ON_COM --source $TRIAGE_SOURCE_TYPE --source-id $TRIAGE_SOURCE_PATH $EXTRA_FLAGS
```

**Pro-tips:**

- For faster feedback, comment out the parts of the policy you don't want to execute
- If you'd like to investigate a particular resource, use a **conditional breakpoint**: `require "byebug"; byebug if resource['iid'] == 420431`
